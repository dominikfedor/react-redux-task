import React, { Component } from 'react';
import './App.css';
import { Sidebar } from "./containers/Sidebar"
import { Table } from "./containers/Table"

class App extends Component {
  render() {
    return (
      <div id="container">
        <Sidebar />
        <section>
          <Table />
        </section>
      </div>
    );
  }
}

export default App;
