import { connect } from 'react-redux'
import TableComponent from '../components/Table'

export const Table = connect(state => ({
	transactions: state.transactions,
	userID: state.userID
}), {})(TableComponent)