import { connect } from 'react-redux'
import SidebarComponent from '../components/Sidebar'
import { loadNewTransactionsList } from '../actions'

/* 
 * mapDispatchToProps
*/
const mapDispatchToProps = dispatch => ({
	dispatch: (userID) => {
	  dispatch(loadNewTransactionsList(userID))
	}
  })

export const Sidebar = connect((state) => ({users: state.users, userID: state.userID}), mapDispatchToProps)(SidebarComponent)