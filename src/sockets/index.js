import { populateUsersList, populateTransactionsList } from '../actions'

export const socket = new WebSocket('wss://stage.decentgo.com:8090/')

const setupSocket = (dispatch) => {
	socket.onopen = () => {
		socket.send(JSON.stringify({ "id": 1, "method": "call", "params": [1, "login", ["", ""]] }))
		socket.send(JSON.stringify({ "id": 2, "method": "call", "params": [1, "history", []] }))
		socket.send(JSON.stringify({ "id": 3, "method": "call", "params": [0, "lookup_accounts", ["", 100]] }))
	}

	socket.onmessage = (event) => {
		const data = JSON.parse(event.data)
		switch (data.id) {
			case 3:
				dispatch(populateUsersList(data.result))
				break
			case 4:
				dispatch(populateTransactionsList(data.result))
				break
			default:
				break
		}

	}
	return socket
}

export default setupSocket
