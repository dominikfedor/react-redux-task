export const populateUsersList = users => ({
	type: "USERS_LIST",
	users
})

export const populateTransactionsList = (transactions, userID) => ({
	type: "TRANSACTIONS_LIST",
	transactions
})

export const loadNewTransactionsList = (userID) => dispatch => {
	dispatch({
		type: 'LOAD_USER_TRANSACTIONS',
		userID
	})
}



