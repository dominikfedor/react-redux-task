import React from 'react'
import propTypes from 'prop-types'
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SendIcon from '@material-ui/icons/Send';

const Sidebar = (props) => (
	<Paper id="sidebar" elevation={4}>
		<List component="nav" elevation={1}>
			{props.users.map(user => (
				<ListItem
					button key={user[1]}
					onClick={() => props.dispatch(user)}>
					{user[1] === props.userID[1] ?
						<ListItemIcon>
							<SendIcon />
						</ListItemIcon> : null}
					<ListItemText inset primary={user[0]} secondary={user[1]} />
				</ListItem>
			))}
		</List>
	</Paper>
)

Sidebar.propTypes = {
	dispatch: propTypes.func.isRequired
}

export default Sidebar