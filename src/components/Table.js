import React from 'react';
import propTypes from 'prop-types';
import TablePaper from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const Table = ({ transactions, userID }) => (
    <div>
        <Typography variant="title" color="inherit">
            {userID[0] ? (userID[0] + " " + (userID[1])) : " Select the account from sidebar"}  
        </Typography>
        <Typography color="inherit">
            {transactions.length ? "Transaction history" : "No transaction history was found!"}
        </Typography>
        <Paper id="table-container">
            <TablePaper >
                <TableHead>
                    <TableRow>
                        <TableCell numeric>ID</TableCell>
                        <TableCell numeric>From</TableCell>
                        <TableCell numeric>To</TableCell>
                        <TableCell numeric>Name</TableCell>
                        <TableCell numeric>Fee</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody id="table-content">
                    {transactions.map(tx => {
                        return (
                            <TableRow key={tx.id}>
                                <TableCell component="th" scope="row">{tx.id}</TableCell>
                                <TableCell numeric>{tx.op[1].from}</TableCell>
                                <TableCell numeric>{tx.op[1].to}</TableCell>
                                <TableCell numeric>{tx.op[1].name}</TableCell>
                                <TableCell numeric>{tx.op[1].fee.amount}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </TablePaper>
        </Paper>
    </div>
);

Table.propTypes = {
    transactions: propTypes.array.isRequired,
    userID: propTypes.array.isRequired
}

export default Table;