
const userID = (state = [], action) => {
	switch (action.type) {
		case "LOAD_USER_TRANSACTIONS":
			return action.userID
		default:
			return state
	}
}

export default userID