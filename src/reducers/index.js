import { combineReducers } from "redux"
import users from "./users"
import transactions from "./transactions"
import userID from "./userID"

export default combineReducers({
	users,
	transactions,
	userID
})
