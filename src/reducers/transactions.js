import { socket } from '../sockets'

const getUserHistory = (userID) => {
	return socket.send(JSON.stringify({ "id": 4, "method": "call", "params": [2, "get_relative_account_history", [userID, 1, 20, 8000]] }));
}

const transactions = (state = [], action) => {
	switch (action.type) {
		case "TRANSACTIONS_LIST":
			return action.transactions
		case "LOAD_USER_TRANSACTIONS":
			getUserHistory(action.userID[1])
			return state
		default:
			return state
	}
}

export default transactions